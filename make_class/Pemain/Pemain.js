class Pemain {
    constructor(nama, negara, nomorPunggung, klub, posisi) {
        this.nama = nama;
        this.negara = negara;
        this.nomorPunggung = nomorPunggung;
        this.klub = klub;
        this.posisi = posisi;
    }

    goal(total) {
        console.log(`${this.nama}, telah mencetak goal sebanyak ${total} pada musim ini`);
    }

    tampil() {
        console.log(`${this.nama} adalah seorang ${this.posisi} saat bermain untuk klub atau untuk negaranya`);
    }
}

module.exports = Pemain;