const Pemain = require('./Pemain/Pemain');

function main() {
    let pemain = new Pemain('Haaland', 'Norwegia', 9, 'Manchester City', 'Penyerang');
    pemain.tampil();
    console.log(`${pemain.nama} yaitu seorang ${pemain.posisi} yang berasal dari negara ${pemain.negara}`);
    console.log(`${pemain.nama} bernomor punggung ${pemain.nomorPunggung}`);
    console.log(`${pemain.nama} sedang menjalani kontrak dengan ${pemain.klub}`);

    pemain.goal(9);
}

main();